<?php
namespace app\components;

use Yii;
use \app\models\Log;
class Logging {
    public function addLog($user, $message, $model, $id, $old_data, $new_data) {
        $log = new Log();
        $log->user = $user;
        $log->message = $message;
        $log->model = $model;
        $log->field_id = $id;
        $log->old_value = json_encode($old_data);
        $log->new_value = json_encode($new_data);
        if($log->validate()) {
            $log->save();
        }
        else {
            var_dump($log->errors);die();
        }

    }
}