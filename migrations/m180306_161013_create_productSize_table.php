<?php

use yii\db\Migration;

/**
 * Handles the creation of table `productSize`.
 * Has foreign keys to the tables:
 *
 * - `product`
 * - `size`
 */
class m180306_161013_create_productSize_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('productSize', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'size_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `product_id`
        $this->createIndex(
            'idx-productSize-product_id',
            'productSize',
            'product_id'
        );

        // add foreign key for table `product`
        $this->addForeignKey(
            'fk-productSize-product_id',
            'productSize',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );

        // creates index for column `size_id`
        $this->createIndex(
            'idx-productSize-size_id',
            'productSize',
            'size_id'
        );

        // add foreign key for table `size`
        $this->addForeignKey(
            'fk-productSize-size_id',
            'productSize',
            'size_id',
            'size',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `product`
        $this->dropForeignKey(
            'fk-productSize-product_id',
            'productSize'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            'idx-productSize-product_id',
            'productSize'
        );

        // drops foreign key for table `size`
        $this->dropForeignKey(
            'fk-productSize-size_id',
            'productSize'
        );

        // drops index for column `size_id`
        $this->dropIndex(
            'idx-productSize-size_id',
            'productSize'
        );

        $this->dropTable('productSize');
    }
}
