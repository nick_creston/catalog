<?php

use yii\db\Migration;

/**
 * Handles adding productId to table `image`.
 * Has foreign keys to the tables:
 *
 * - `product`
 */
class m180306_160906_add_productId_column_to_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('image', 'product_id', $this->integer());

        // creates index for column `product_id`
        $this->createIndex(
            'idx-image-product_id',
            'image',
            'product_id'
        );

        // add foreign key for table `product`
        $this->addForeignKey(
            'fk-image-product_id',
            'image',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `product`
        $this->dropForeignKey(
            'fk-image-product_id',
            'image'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            'idx-image-product_id',
            'image'
        );

        $this->dropColumn('image', 'product_id');
    }
}
