<?php

use yii\db\Migration;

/**
 * Handles the creation of table `log`.
 */
class m180312_062532_create_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('log', [
            'id' => $this->primaryKey(),
            'created' => $this->timestamp(),
            'user' => $this->string(),
            'message' => $this->string(),
            'model' => $this->string(),
            'field_id' => $this->integer(),
            'old_value' => $this->text(),
            'new_value' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('log');
    }
}
