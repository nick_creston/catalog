<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 * Has foreign keys to the tables:
 *
 * - `category`
 * - `image`
 */
class m180306_160715_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'category_id' => $this->integer()->notNull(),
            'preview_id' => $this->integer(),
        ]);

        // creates index for column `category_id`
        $this->createIndex(
            'idx-product-category_id',
            'product',
            'category_id'
        );

        // add foreign key for table `category`
        $this->addForeignKey(
            'fk-product-category_id',
            'product',
            'category_id',
            'category',
            'id',
            'CASCADE'
        );

        // creates index for column `preview_id`
        $this->createIndex(
            'idx-product-preview_id',
            'product',
            'preview_id'
        );

        // add foreign key for table `image`
        $this->addForeignKey(
            'fk-product-preview_id',
            'product',
            'preview_id',
            'image',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `category`
        $this->dropForeignKey(
            'fk-product-category_id',
            'product'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            'idx-product-category_id',
            'product'
        );

        // drops foreign key for table `image`
        $this->dropForeignKey(
            'fk-product-preview_id',
            'product'
        );

        // drops index for column `preview_id`
        $this->dropIndex(
            'idx-product-preview_id',
            'product'
        );

        $this->dropTable('product');
    }
}
