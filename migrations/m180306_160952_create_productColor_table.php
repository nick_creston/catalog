<?php

use yii\db\Migration;

/**
 * Handles the creation of table `productColor`.
 * Has foreign keys to the tables:
 *
 * - `product`
 * - `color`
 */
class m180306_160952_create_productColor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('productColor', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'color_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `product_id`
        $this->createIndex(
            'idx-productColor-product_id',
            'productColor',
            'product_id'
        );

        // add foreign key for table `product`
        $this->addForeignKey(
            'fk-productColor-product_id',
            'productColor',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );

        // creates index for column `color_id`
        $this->createIndex(
            'idx-productColor-color_id',
            'productColor',
            'color_id'
        );

        // add foreign key for table `color`
        $this->addForeignKey(
            'fk-productColor-color_id',
            'productColor',
            'color_id',
            'color',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `product`
        $this->dropForeignKey(
            'fk-productColor-product_id',
            'productColor'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            'idx-productColor-product_id',
            'productColor'
        );

        // drops foreign key for table `color`
        $this->dropForeignKey(
            'fk-productColor-color_id',
            'productColor'
        );

        // drops index for column `color_id`
        $this->dropIndex(
            'idx-productColor-color_id',
            'productColor'
        );

        $this->dropTable('productColor');
    }
}
