Catalog Test App
-------------------

Yii 2 Basic Project Template.

**Installation**

1. git clone 
2. composer install
3. create database catalog with username _catalog_ and password _catalog_ and 
grant all privileges
4. run _./yii migrate_ for create tables
5. run _./yii migrate --migrationPath=@app/demo_migrations_ for demo data