<?php

namespace app\controllers;

use app\models\Category;
use app\models\Log;
use app\models\Product;
use app\models\ProductForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $categories = Category::find()->all();
        $products = Product::find()->orderBy(['id'=>SORT_DESC])->all();
        return $this->render('index', [
            'categories' => $categories,
            'products' => $products
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Display product page
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionProduct($id) {
        $product = Product::findOne($id);
        if(!$product) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $logs = Log::find()
            ->where(['model'=> ProductForm::class, 'field_id'=> $product->id])
            ->orderBy(['created'=> SORT_DESC])
            ->all();
        return $this->render('product', [
            'product' => $product,
            'colors' => $product->colors,
            'sizes' => $product->sizes,
            'photos' => $product->images,
            'logs' => $logs
        ]);
    }

    public function actionCategory($id) {
        $category = Category::findOne($id);
        if(!$category) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $categories = Category::find()->all();
        $products = Product::find()->where(['category_id'=> $category->id])->orderBy(['id'=>SORT_DESC])->all();
        return $this->render('category', [
            'categories' => $categories,
            'current_category' => $category,
            'products' => $products,
        ]);
    }
}
