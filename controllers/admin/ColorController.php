<?php
namespace app\controllers\admin;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\Color;
use yii\web\NotFoundHttpException;

class ColorController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex() {
        $colors = new ActiveDataProvider([
            'query' => Color::find(),
        ]);
        return $this->render('index', [
            'colors' => $colors
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $color = new Color();
        if ($color->load(Yii::$app->request->post()) && $color->save()) {
            Yii::$app->logging->addLog(
                Yii::$app->user->username,
                "Create color {$color->name}",
                Color::class, $color->id,
                null, $color->toArray());
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'color' => $color,
            ]);
        }
    }

    /**
     * @param integer $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id) {
        $color = $this->findModel((int)$id);
        $old_data = $color->toArray();

        if ($color->load(Yii::$app->request->post()) && $color->save()) {
            Yii::$app->logging->addLog(
                Yii::$app->user->username,
                "Update color {$old_data['name']} => {$color->name}",
                Color::class,
                $old_data['id'],
                $old_data,
                $color->toArray());
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'color' => $color,
            ]);
        }
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $old_data = $model->toArray();
        $model->delete();

        Yii::$app->logging->addLog(Yii::$app->user->username, "Delete color {$old_data['name']}",
            Color::class,
            $old_data['id'],
            $old_data, null);

        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     * @return Color
     * @throws NotFoundHttpException
     */
    protected function findModel($id) {
        if (($color = Color::findOne($id)) !== null) {
            return $color;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}