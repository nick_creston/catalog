<?php
namespace app\controllers\admin;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\Log;
use yii\web\NotFoundHttpException;

class LogsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex() {
        $logs = new ActiveDataProvider([
            'query' => Log::find()->orderBy(['created'=> SORT_DESC]),
        ]);
        return $this->render('index', [
            'logs' => $logs
        ]);
    }

    public function actionView($id) {
        $log = Log::findOne($id);
        if(!$log) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->render('view', ['log'=> $log]);
    }
}