<?php

namespace app\controllers\admin;


use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\Product;
use app\models\ProductForm;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;

class ProductController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex() {
        $products = new ActiveDataProvider([
            'query' => Product::find(),
        ]);
        return $this->render('index', [
            'products' => $products
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $product = new ProductForm();
        if ($product->load(Yii::$app->request->post())) {
            if($product->save()) {
                $product->saveColors();
                $product->saveSizes();
                $product->imageFiles = UploadedFile::getInstances($product, 'imageFiles');
                $product->upload();
                Yii::$app->logging->addLog(
                    Yii::$app->user->username,
                    "Create product {$product->name}",
                    ProductForm::class,
                    $product->id,
                    null,
                    $this->getFullInfoProduct($product->id));
                return $this->redirect(['index']);
            }

        } else {
            return $this->render('create', [
                'product' => $product,
            ]);
        }
    }

    /**
     * @param integer $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id) {
        $product = $this->findModel((int)$id);
        $old_data = $this->getFullInfoProduct($product->id);
        $product->loadColors();
        $product->loadSizes();


        if ($product->load(Yii::$app->request->post())) {

            if($product->save()) {
                $product->saveColors();
                $product->saveSizes();
                $product->imageFiles = UploadedFile::getInstances($product, 'imageFiles');
                $product->upload();

                Yii::$app->logging->addLog(
                    Yii::$app->user->username,
                    "Update product {$product->name}",
                    ProductForm::class,
                    $product->id,
                    $old_data,
                    $this->getFullInfoProduct($product->id));

                return $this->redirect(['index']);
            }

        } else {
            return $this->render('update', [
                'product' => $product,
            ]);
        }
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $product = $this->findModel($id);
        $old_data = $this->getFullInfoProduct($product->id);
        $product->delete();

        Yii::$app->logging->addLog(
            Yii::$app->user->username,
            "Delete product {$old_data['name']}",
            ProductForm::class,
            $old_data['id'],
            $old_data,
            null);
        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     * @return ProductForm
     * @throws NotFoundHttpException
     */
    protected function findModel($id) {
        if (($product = ProductForm::findOne($id)) !== null) {
            return $product;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getFullInfoProduct($id) {
        $product = $this->findModel($id);
        $result = [];
        if($product) {
            $result = $product->toArray();
            $result['colors'] = ArrayHelper::toArray($product->colors);
            $result['sizes'] = ArrayHelper::toArray($product->sizes);
            $result['files'] = ArrayHelper::toArray($product->images);
        }
        return $result;

    }
}