<?php
namespace app\controllers\admin;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\Category;
use yii\web\NotFoundHttpException;

class CategoryController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex() {
        $categories = new ActiveDataProvider([
            'query' => Category::find(),
        ]);
        return $this->render('index', [
            'categories' => $categories
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $category = new Category();
        if ($category->load(Yii::$app->request->post()) && $category->save()) {
            Yii::$app->logging->addLog(
                Yii::$app->user->username,
                "Create category {$category->name}",
                Category::class,
                $category->id,
                null,
                $category->toArray());
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'category' => $category,
            ]);
        }
    }

    /**
     * @param integer $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id) {
        $category = $this->findModel((int)$id);
        $old_data = $category->toArray();

        if ($category->load(Yii::$app->request->post()) && $category->save()) {
            Yii::$app->logging->addLog(
                Yii::$app->user->username,
                "Update category {$old_data['name']} => {$category->name}",
                Category::class,
                $category->id,
                $old_data,
                $category->toArray());
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'category' => $category,
            ]);
        }
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $category = $this->findModel($id);
        $old_data = $category->toArray();
        $category->delete();

        Yii::$app->logging->addLog(
            Yii::$app->user->username,
            "Delete category {$old_data['name']}",
            Category::class,
            $category->id,
            $old_data,
            null);

        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     * @return Category
     * @throws NotFoundHttpException
     */
    protected function findModel($id) {
        if (($category = Category::findOne($id)) !== null) {
            return $category;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}