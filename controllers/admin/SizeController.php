<?php
namespace app\controllers\admin;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\Size;
use yii\web\NotFoundHttpException;

class SizeController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex() {
        $sizes = new ActiveDataProvider([
            'query' => Size::find(),
        ]);
        return $this->render('index', [
            'sizes' => $sizes
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $size = new Size();
        if ($size->load(Yii::$app->request->post()) && $size->save()) {
            Yii::$app->logging->addLog(
                Yii::$app->user->username,
                "Create size {$size->name}",
                Size::class,
                $size->id,
                null,
                $size->toArray());
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'size' => $size,
            ]);
        }
    }

    /**
     * @param integer $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate($id) {
        $size = $this->findModel((int)$id);
        $old_data = $size->toArray();

        if ($size->load(Yii::$app->request->post()) && $size->save()) {
            Yii::$app->logging->addLog(
                Yii::$app->user->username,
                "Update size {$old_data['name']} => {$size->name}",
                Size::class,
                $old_data['id'],
                $old_data,
                $size->toArray());
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'size' => $size,
            ]);
        }
    }

    /**
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $size = $this->findModel($id);
        $old_data = $size->toArray();
        $size->delete();
        Yii::$app->logging->addLog(
            Yii::$app->user->username,
            "Delete size {$old_data['name']}",
            Size::class,
            $old_data['id'],
            $old_data,
            null);

        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     * @return Size
     * @throws NotFoundHttpException
     */
    protected function findModel($id) {
        if (($size = Size::findOne($id)) !== null) {
            return $size;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}