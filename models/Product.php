<?php
namespace app\models;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


/**
 * Class Product
 * @package app\models
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 * @property string $preview_id
 */
class Product extends ActiveRecord
{
    public static function tableName()
    {
        return 'product';
    }

    public function rules() {
        return [
            [['name'], 'string', 'max'=> 255],
            [['category_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category_id' => 'Category'
        ];
    }

    public function getCategory() {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    public function getCategoryList() {
        $categories = Category::find()->asArray()->all();
        return ArrayHelper::map($categories, 'id', 'name');
    }

    public function getColors() {
        return $this->hasMany(Color::class, ['id' => 'color_id'])
            ->viaTable('productColor', ['product_id' => 'id']);
    }
    public function getColorsList() {
        $colors = Color::find()->asArray()->all();
        return ArrayHelper::map($colors, 'id', 'name');
    }

    public function getSizes() {
        return $this->hasMany(Size::class, ['id' => 'size_id'])
            ->viaTable('productSize', ['product_id' => 'id']);
    }
    public function getSizesList() {
        $sizes = Size::find()->asArray()->all();
        return ArrayHelper::map($sizes, 'id', 'name');
    }
    public function getImages() {
        return $this->hasMany(Image::class, ['product_id' => 'id']);
    }
}
