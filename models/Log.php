<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * Class Log
 * @package app\models
 * @property datetime created
 * @property string $user
 * @property string message
 * @property string model
 * @property integer $field_id
 * @property string old_value
 * @property string new_value
 */
class Log extends ActiveRecord
{
//    public $created;
//    public $user;
//    public $message;
//    public $old_value;
//    public $new_value;

    public static function tableName()
    {
        return 'log';
    }

    public function rules() {
        return [
            [['user'], 'string'],
            [['message'], 'string'],
            [['model'], 'string'],
            [['field_id'], 'integer'],
            [['old_value'], 'string'],
            [['new_value'], 'string'],
        ];
    }


    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created' => 'Created',
            'message' => 'Message'
        ];
    }
}