<?php

namespace app\models;
use Yii;
use yii\db\ActiveRecord;

class ProductColors extends ActiveRecord
{
    public static function tableName()
    {
        return 'productColor';
    }

    public function rules() {
        return [
            [['product_id'], 'integer'],
            [['color_id'], 'integer']
        ];
    }
}