<?php
namespace app\models;
use Yii;
use yii\db\ActiveRecord;
/**
 * Class Category
 * @package app\models
 * @property integer $id
 * @property string $name
 */
class Size extends ActiveRecord
{
    public static function tableName()
    {
        return 'size';
    }

    public function rules() {
        return [
            [['name'], 'string', 'max'=> 50]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name'
        ];
    }
}