<?php
namespace app\models;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class ProductForm extends Product
{
    /**
     * @var array IDs of colors
     */
    public $color_ids = [];
    /**
     * @var array IDs of sizes
     */
    public $size_ids = [];

    /**
     * @var UploadedFile[]
     */
    public $imageFiles;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            // each category_id must exist in category table (*1)
            ['color_ids', 'each', 'rule' => [
                'exist', 'targetClass' => Color::class, 'targetAttribute' => 'id'
            ]
            ],
            ['size_ids', 'each', 'rule' => [
                'exist', 'targetClass' => Size::class, 'targetAttribute' => 'id'
            ]
            ],
            [['imageFiles'], 'file', 'extensions' => 'png, jpg', 'maxFiles' => 4],
        ]);
    }
    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'color_ids' => 'Colors',
            'size_ids' => 'Sizes'
        ]);
    }

    public function loadColors() {
        $this->color_ids = [];
        if(!empty($this->id)) {
            $colors = ProductColors::find()
                ->select(['color_id'])
                ->where(['product_id' => $this->id])
                ->asArray()
                ->all();
            foreach($colors as $color) {
                $this->color_ids[] = $color['color_id'];
            }
        }
    }

    public function saveColors() {
        ProductColors::deleteAll(['product_id' => $this->id]);
        if (is_array($this->color_ids)) {
            foreach($this->color_ids as $color_id) {
                $pc = new ProductColors();
                $pc->product_id = $this->id;
                $pc->color_id = $color_id;
                $pc->save();
            }
        }
    }

    public function loadSizes() {
        $this->size_ids = [];
        if(!empty($this->id)) {
            $colors = ProductSizes::find()
                ->select(['size_id'])
                ->where(['product_id' => $this->id])
                ->asArray()
                ->all();
            foreach($colors as $color) {
                $this->size_ids[] = $color['size_id'];
            }
        }
    }

    public function saveSizes() {
        ProductSizes::deleteAll(['product_id' => $this->id]);
        if (is_array($this->size_ids)) {
            foreach($this->size_ids as $size_id) {
                $pc = new ProductSizes();
                $pc->product_id = $this->id;
                $pc->size_id = $size_id;
                $pc->save();
            }
        }
    }

    public function loadImages() {

    }

    public function upload() {
        if ($this->validate()) {
            foreach ($this->imageFiles as $file) {
                $path = 'uploads/' . $file->baseName . '.' . $file->extension;
                $file->saveAs($path);
                $im = new Image();
                $im->path = $path;
                $im->product_id = $this->id;
                $im->save();
            }
            return true;
        } else {
            return false;
        }
    }

}