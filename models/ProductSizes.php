<?php

namespace app\models;
use Yii;
use yii\db\ActiveRecord;

class ProductSizes extends ActiveRecord
{
    public static function tableName()
    {
        return 'productSize';
    }

    public function rules() {
        return [
            [['product_id'], 'integer'],
            [['size_id'], 'integer']
        ];
    }
}