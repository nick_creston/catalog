<?php
namespace app\models;
use Yii;
use yii\db\ActiveRecord;
/**
 * Class Category
 * @package app\models
 * @property integer $id
 * @property string $name
 */
class Color extends ActiveRecord
{
    public static function tableName()
    {
        return 'color';
    }

    public function rules() {
        return [
            [['name'], 'string', 'max'=> 50]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name'
        ];
    }
}