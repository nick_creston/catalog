<?php
namespace app\models;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class Image
 * @package app\models
 * @property string path
 * @property integer product_id
 */
class Image extends ActiveRecord
{
    public static function tableName()
    {
        return 'image';
    }

    public function rules() {
        return [
            [['product_id', 'path'], 'required'],
            [['product_id'], 'integer'],
            [['path'], 'file', 'extensions' =>  'png, jpg, gif',],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'product_id' => 'Product'
        ];
    }
}