<?php

use yii\db\Migration;

/**
 * Class m180312_174525_insert_size_table
 */
class m180312_174525_insert_size_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('size', [
            'name' => 'M',
        ]);
        $this->insert('size', [
            'name' => 'L',
        ]);
        $this->insert('size', [
            'name' => 'XL',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180312_174525_insert_size_table cannot be reverted.\n";

        return false;
    }
}
