<?php

use yii\db\Migration;

/**
 * Class m180312_174631_insert_category_table
 */
class m180312_174631_insert_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('category', [
            'name' => 'Jean',
        ]);
        $this->insert('category', [
            'name' => 'Sweater',
        ]);
        $this->insert('category', [
            'name' => 'Hat',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180312_174631_insert_category_table cannot be reverted.\n";

        return false;
    }
}
