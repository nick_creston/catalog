<?php

use yii\db\Migration;

/**
 * Class m180312_174311_insert_color_table
 */
class m180312_174311_insert_color_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('color', [
            'name' => 'Red',
        ]);
        $this->insert('color', [
            'name' => 'Green',
        ]);
        $this->insert('color', [
            'name' => 'Blue',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180312_174311_insert_color_table cannot be reverted.\n";

        return false;
    }
}
