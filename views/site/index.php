<?php

/* @var $this yii\web\View */

$this->title = 'Catalog';
?>
<div class="container-fluid">
    <div class="row">
        <div id="navbar2" class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <?php foreach($categories as $category):?>
                    <li><a href="/category/<?= $category->id ?>"><?=$category->name?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="col-sm-9 col-md-10">
            <h3>list of products</h3>
            <div class="row">
                <?php foreach($products as $product):?>
                    <?php
                    $photos = \yii\helpers\ArrayHelper::toArray($product->images);
                    ?>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img src="<?php if( isset($photos[0]) ):?>/<?=$photos[0]['path']?><?php else:?>http://placehold.it/350x222<?php endif;?>" style="height: 222px" alt="">
                        <div class="caption">
                            <h4><a href="/product/<?=$product->id?>"><?=$product->name?></a></h4>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
