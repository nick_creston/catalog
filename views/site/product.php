<?php

/* @var $this yii\web\View */

$this->title = 'Catalog - Product '.$product->name;
//$photos = \yii\helpers\ArrayHelper::toArray($images);
?>
<style>
    main .section {
        padding: 40px 0;
    }
    main .section.section-gray {
        background: #eee;
    }
    main .section .section-title {
        text-align: center;
        font-size: 20px;
        margin-bottom: 25px;
    }
    main .section .section-content {
        width: 70%;
        max-width: 1024px;
        margin: 0 auto;
        position: relative;
    }
    @media only screen and (max-width: 959px) {
        main .section .section-content {
            width: calc(100% - 80px);
        }
    }

    /** Product details */
    .product-details {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -ms-flex-pack: distribute;
        justify-content: space-around;
    }
    .product-details .product-images {
        width: 50%;
    }
    @media only screen and (max-width: 959px) {
        .product-details .product-images {
            width: 100%;
        }
    }
    .product-details .product-images > li {
        display: inline-block;
        width: 64px;
        height: product-dimenstions(64px);
        overflow: hidden;
        margin: 5px;
    }
    .product-details .product-images > li.preview {
        width: 100%;
        height: auto;
        margin: 0;
    }
    .product-details .product-images img {
        display: block;
        width: 100%;
    }
    .product-details .product-info {
        width: 40%;
        margin-left: 10%;
    }
    @media only screen and (max-width: 959px) {
        .product-details .product-info {
            width: 100%;
            margin-left: 0;
        }
    }
    .product-details .product-info > li {
        margin: 10px 0;
    }
    .product-details .product-info .product-name {
        font-size: 20px;
        font-weight: bold;
    }
    .product-details .product-info .product-price {
        font-size: 18px;
        color: #666;
    }
    .product-details .product-info .product-attributes {
        width: 66%;
        margin-top: 40px;
    }
    .product-details .product-info .product-addtocart {
        width: 66%;
        margin: 20px 0 40px;
    }
    @media only screen and (max-width: 959px) {
        .product-details .product-info .product-addtocart {
            width: 33%;
        }
    }
    .product-details .product-info .product-addtocart button {
        width: 100%;
        cursor: pointer;
        background: #000;
        color: #fff;
        display: block;
        border: none;
        outline: none;
        padding: 10px;
    }
    .product-details .product-info .product-addtocart button:hover {
        background: #1a1a1a;
    }
    .product-details .product-info .product-description {
        font-size: 12px;
    }

    /** Product list */
    .product-list {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
    }

    /** Product */
    .product {
        display: block;
        width: 150px;
        height: calc($value + $value * 0.1);
        margin: 5px;
        overflow: hidden;
        text-align: center;
    }
    @media only screen and (max-width: 767px) {
        .product {
            width: 280px;
            height: calc($value + $value * 0.1);
        }
    }
    @media only screen and (min-width: 1359px) {
        .product {
            width: 210px;
            height: calc($value + $value * 0.1);
        }
    }
    .product .product-image {
        background: #eee;
    }
    .product .product-image img {
        display: block;
        width: 100%;
    }
    .product .product-name {
        font-weight: bold;
        margin: 10px 0 5px;
    }

    a.product {
        color: #000;
        text-decoration: none;
    }

    /** Fields and forms */
    .fields > li {
        margin-bottom: 10px;
    }
    .fields .field-name {
        display: block;
        font-weight: bold;
        margin-bottom: 5px;
    }

    label {
        cursor: pointer;
        white-space: nowrap;
    }
</style>
<main>
    <div class="section section-gray">
        <div class="section-content">
            <div class="product-details">
                <?php if($photos):?>
                <ul class="product-images">
                    <li class="preview"><img src="/<?=$photos[0]->path?>" alt=""></li>
                    <!-- Don't show small pictures if there's only 1 -->
                    <?php if( count($photos) > 1 ): ?>
                    <?php foreach ($photos as $photo): ?>
                    <li>
                        <a href="javascript:void(0)"><img src="/<?=$photo->path?>" alt=""></a>
                    </li>
                    <?php endforeach;?>

                    <?php endif;?>
                </ul>
                <?php endif;?>
                <ul class="product-info">
                    <li class="product-name"><?=$product->name?></li>

                    <li class="product-attributes">
                        <ul class="fields">
                            <li>
                                <div class="field-name">Color:</div>
                                <?php foreach ($colors as $color): ?>
                                <label>
                                    <input type="radio" name="color"> <?=$color->name?>
                                </label>
                                <?php endforeach;?>
                            </li>
                            <li>
                                <div class="field-name">Size:</div>
                                <?php foreach ($sizes as $size): ?>
                                    <label>
                                        <input type="radio" name="size"> <?=$size->name?>
                                    </label>
                                <?php endforeach;?>
                            </li>
                        </ul>
                    </li>
                    <li class="product-addtocart">
                        <button>Add To Cart</button>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</main>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>
                            Created
                        </th>
                        <th>
                            Message
                        </th>
                    </tr>
                    <tbody>
                    <?php foreach ($logs as $log): ?>
                        <tr>
                            <td><?= $log->created ?></td>
                            <td><?= $log->message ?></td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                    </thead>
                </table>
            </div>
        </div>

    </div>
</section>