<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = 'Admin - Catalog';
?>
<div class="container">
  <div class="row">
    <div class="col-md-9">
      <?= Html::a('Color', ['admin/color'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Size', ['admin/size'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Category', ['admin/category'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Product', ['admin/product'], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Logs', ['admin/logs'], ['class' => 'btn btn-primary']) ?>

    </div>
  </div>
</div>