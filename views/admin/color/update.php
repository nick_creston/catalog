<?php
/* @var $this yii\web\View */
/* @var $color app\models\Color */
$this->title = 'Admin - Color';
?>
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <?= $this->render('_form', [
                'color' => $color,
            ]) ?>
        </div>
    </div>
</div>

