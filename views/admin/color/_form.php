<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $color app\models\Color */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<?= $form->field($color, 'name')->textInput() ?>
<div class="form-group">
    <?= Html::submitButton($color->isNewRecord ? 'Create' : 'Edit', ['class' => $color->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
