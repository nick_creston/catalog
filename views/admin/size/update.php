<?php
/* @var $this yii\web\View */
/* @var $size app\models\Size */
$this->title = 'Admin - Edit Size';
?>
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <?= $this->render('_form', [
                'size' => $size,
            ]) ?>
        </div>
    </div>
</div>

