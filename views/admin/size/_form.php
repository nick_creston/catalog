<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $size app\models\Size */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<?= $form->field($size, 'name')->textInput() ?>
<div class="form-group">
    <?= Html::submitButton($size->isNewRecord ? 'Create' : 'Edit', ['class' => $size->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
