<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $sizes yii\data\ActiveDataProvider */
$this->title = 'Admin - Sizes';
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Sizes</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= Html::a('Add size', ['create'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-9">
            <?= GridView::widget([
                'dataProvider' => $sizes,
                'columns' => [
                    'id',
                    'name',

                    ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
                ],
            ]); ?>
        </div>
    </div>
</div>
