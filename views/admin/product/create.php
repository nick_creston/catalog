<?php
/* @var $this yii\web\View */
/* @var $product app\models\Product */
$this->title = 'Admin - Add Product';
?>
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <?= $this->render('_form', [
                'product' => $product,
            ]) ?>
        </div>
    </div>
</div>
