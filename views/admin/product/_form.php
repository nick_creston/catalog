<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $product app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
<?= $form->field($product, 'name')->textInput() ?>
<?= $form->field($product, 'category_id')->dropDownList($product->getCategoryList()) ?>
<?= $form->field($product, 'color_ids')->dropDownList($product->getColorsList(),  ['multiple'=>'multiple',]) ?>
<?= $form->field($product, 'size_ids')->dropDownList($product->getSizesList(),  ['multiple'=>'multiple',]) ?>
<div class="form-group">
    <?= FileInput::widget([
        'model' => $product,
        'attribute' => 'imageFiles[]',
        'options' => ['multiple' => true, 'accept' => 'image/*'],
        'pluginOptions' => [
            'showUpload' => false,
        ]

    ]);
    ?>
</div>

<div class="form-group">
    <?= Html::submitButton($product->isNewRecord ? 'Create' : 'Edit', ['class' => $product->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>
