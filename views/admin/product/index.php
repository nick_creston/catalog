<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $products yii\data\ActiveDataProvider */
$this->title = 'Admin - Products';
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Products</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= Html::a('Add product', ['create'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-9">
            <?= GridView::widget([
                'dataProvider' => $products,
                'columns' => [
                    'id',
                    'name',

                    ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
                ],
            ]); ?>
        </div>
    </div>
</div>
