<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
$this->title = 'Admin - Logs - View';
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Logs view</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th>created</th>
                    <td><?=$log->created?></td>
                </tr>
                <tr>
                    <th>message</th>
                    <td><?=$log->message?></td>
                </tr>
                <tr>
                    <th>model</th>
                    <td><?=$log->model?></td>
                </tr>
                <tr>
                    <th>field id</th>
                    <td><?=$log->field_id?></td>
                </tr>
                <tr>
                    <th>old data</th>
                    <td><div style="font-family: 'Courier New', Courier, monospace"><?=$log->old_value?></div></td>
                </tr>
                <tr>
                    <th>new data</th>
                    <td><div style="font-family: 'Courier New', Courier, monospace"><?=$log->new_value?></div></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

