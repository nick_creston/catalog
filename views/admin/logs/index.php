<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $logs yii\data\ActiveDataProvider */

$this->title = 'Admin - Logs';
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Logs</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <?= GridView::widget([
                'dataProvider' => $logs,
                'columns' => [
                    'created',
                    'message',

                    ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
                ],
            ]); ?>
        </div>
    </div>
</div>

