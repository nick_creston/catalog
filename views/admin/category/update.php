<?php
/* @var $this yii\web\View */
/* @var $color app\models\Category */
$this->title = 'Admin - Category';
?>
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <?= $this->render('_form', [
                'category' => $category,
            ]) ?>
        </div>
    </div>
</div>

